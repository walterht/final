<%@ page import="com.example.inventorymanagement.UserEntity" %>
<%@ page import="com.mysql.cj.log.Log" %><%--
  Created by IntelliJ IDEA.
  User: walta
  Date: 3/23/2022
  Time: 8:20 PM
  To change this template use File | Settings | File Templates.
--%>
<header>
    <img src="${pageContext.request.contextPath}/images/logo.png" alt="PHP Motors Logo">
    <div id="header-right">
        <c:if test="${sessionScope.user != null}">
            <span id='welcome-span'>Welcome ${sessionScope.user.getUserFirstname()}</span>
        </c:if>
        <c:choose>
            <c:when test="${(sessionScope.LoggedIn == null) || (sessionScope.LoggedIn == false)}">
                    <a id='account-link' title='Login link' href='${pageContext.request.contextPath}/account?action=login'>Login</a>
            </c:when>
            <c:when test="${(sessionScope.LoggedIn != null) && (sessionScope.LoggedIn == true)}">
                <a id='account-link' title='Logout of Your Account' href='${pageContext.request.contextPath}/account?action=logout'>Log Out</a>
            </c:when>
        </c:choose>
    </div>
</header>