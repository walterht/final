<%--
  Created by IntelliJ IDEA.
  User: walta
  Date: 3/23/2022
  Time: 9:07 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login | Inventory Management</title>
    <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/home.css">
</head>
<body>

    <%@ include file="../snippets/header.jsp"%>

    <nav></nav>

    <main>
        <h1>Log In</h1>

        <c:if test="${sessionScope.message != null}"><p>${sessionScope.message}</p><c:remove var="message" scope="session"/></c:if>
        <c:if test="${requestScope.message != null}"><p>${requestScope.message}</p><c:remove var="message" scope="request"/></c:if>

        <form method="post" action="${pageContext.request.contextPath}/account">
            <label for="email">Email:</label>
            <input name="clientEmail" id="email" type="email" value="${requestScope.email}" required>
            <label for="password">Password:</label>
            <span>Passwords must be at least 8 characters and contain at least 1 number, 1 capital letter and 1 special character</span>
            <input name="clientPassword" id="password" type="password" required pattern="(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">
            <input type="submit" name="submit" id="form_btn" value="Log In">
            <input type="hidden" name="action" value="Login">
        </form>
        <p class="signup_p">Not Registered? <a class="signup_p content-link" href="${pageContext.request.contextPath}/account?action=registration">Sign-up</a></p>
    </main>

    <%@ include file="../snippets/footer.jsp"%>
</body>
</html>
