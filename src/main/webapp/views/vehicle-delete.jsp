<%--
  Created by IntelliJ IDEA.
  User: walta
  Date: 3/29/2022
  Time: 12:35 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Delete ${requestScope.vehicleMake} ${requestScope.vehicleModel} | Inventory Management</title>
    <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/home.css">
</head>
<body>

    <%@ include file="../snippets/header.jsp"%>

    <nav>
        <c:if test="${requestScope.navigationList != null}">
            ${requestScope.navigationList}
            <c:remove var="navigationList" scope="request"/>
        </c:if>
    </nav>

    <main>
        <h1>Delete ${requestScope.vehicleMake} ${requestScope.vehicleModel}</h1>

        <p>Confirm Vehicle Deletion. The deletion is permanent.</p>
        <form method="post" action="${pageContext.request.contextPath}/vehicles">
            <label for="invMake">Make:</label>
            <input type="text" name="invMake" id="invMake" value="${requestScope.vehicleMake}" readonly>
            <label for="invModel">Model:</label>
            <input type="text" name="invModel" id="invModel" value="${requestScope.vehicleModel}" readonly>
            <label for="invDescription">Description:</label>
            <textarea name="invDescription" id="invDescription" readonly>${requestScope.description}</textarea>
            <input type="submit" name="submit" id="form_btn" value="Delete Vehicle">
            <input type="hidden" name="action" value="deleteVehicle">
            <input type="hidden" name="vehicleId" value="${requestScope.vehicleId}">
        </form>
    </main>

    <%@ include file="../snippets/footer.jsp"%>

</body>
</html>
