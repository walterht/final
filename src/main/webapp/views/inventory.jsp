<%--
  Created by IntelliJ IDEA.
  User: walta
  Date: 3/28/2022
  Time: 7:58 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${requestScope.classificationName} | Inventory Management</title>
    <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/home.css">
</head>
<body>
    <%@ include file="../snippets/header.jsp"%>

    <nav>
        <c:if test="${requestScope.navigationList != null}">
            ${requestScope.navigationList}
            <c:remove var="navigationList" scope="request"/>
        </c:if>
    </nav>

    <main>
        <h1>${requestScope.classificationName} Inventory</h1>

        <table>${requestScope.invDisplay}</table>


        <a class="content-link" href="vehicles?action=add&classificationName=${requestScope.classificationName}">Add Vehicle</a>
    </main>

    <%@ include file="../snippets/footer.jsp"%>

</body>
</html>
