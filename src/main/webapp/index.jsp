<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome | Inventory Management</title>
    <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/home.css">
</head>
<body>
    <%@include file="snippets/header.jsp"%>

    <nav></nav>

    <main>
        <h1>Welcome to the Vehicle Inventory Management Application</h1>

        <p>Please login to the application to be able to use it.</p>
    </main>

    <%@include file="snippets/footer.jsp"%>
</body>
</html>