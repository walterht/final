package com.example.inventorymanagement;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class HibernateFactory {

    private static final SessionFactory sf = buildSessionFactory();

    private static SessionFactory buildSessionFactory(){
        try {
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()//
                    .configure("hibernate.cfg.xml").build();

            Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();

            return metadata.getSessionFactoryBuilder().build();
        }
        catch (Throwable e){
            System.err.println("Initial Session Factory creation failed. " + e);
            throw new ExceptionInInitializerError(e);
        }
    }

    public static SessionFactory getSessionFactory(){
        return sf;
    }

    public static void shutdown(){
        getSessionFactory().close();
    }

}
