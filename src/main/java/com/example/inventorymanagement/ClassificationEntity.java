package com.example.inventorymanagement;

import javax.persistence.*;

@Entity
@Table(name = "classification", schema = "inventory")
public class ClassificationEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "classificationID")
    private int classificationId;
    @Basic
    @Column(name = "classificationName")
    private String classificationName;

    public int getClassificationId() {
        return classificationId;
    }

    public void setClassificationId(int classificationId) {
        this.classificationId = classificationId;
    }

    public String getClassificationName() {
        return classificationName;
    }

    public void setClassificationName(String classificationName) {
        this.classificationName = classificationName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClassificationEntity that = (ClassificationEntity) o;

        if (classificationId != that.classificationId) return false;
        if (classificationName != null ? !classificationName.equals(that.classificationName) : that.classificationName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = classificationId;
        result = 31 * result + (classificationName != null ? classificationName.hashCode() : 0);
        return result;
    }
}
