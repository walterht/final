package com.example.inventorymanagement;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Entity
@Table(name = "vehicle", schema = "inventory")
public class VehicleEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "vehicleID")
    private int vehicleId;
    @Basic
    @Column(name = "invMake")
    private String invMake;
    @Basic
    @Column(name = "invModel")
    private String invModel;
    @Basic
    @Column(name = "invDescription")
    private String invDescription;
    @Basic
    @Column(name = "invPrice")
    private BigDecimal invPrice;
    @Basic
    @Column(name = "invStock")
    private int invStock;
    @Basic
    @Column(name = "invColor")
    private String invColor;
    @Basic
    @Column(name = "classificationID")
    private int classificationId;

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getInvMake() {
        return invMake;
    }

    public void setInvMake(String invMake) {
        this.invMake = invMake;
    }

    public String getInvModel() {
        return invModel;
    }

    public void setInvModel(String invModel) {
        this.invModel = invModel;
    }

    public String getInvDescription() {
        return invDescription;
    }

    public void setInvDescription(String invDescription) {
        this.invDescription = invDescription;
    }

    public BigDecimal getInvPrice() {
        return invPrice;
    }

    public void setInvPrice(BigDecimal invPrice) {
        this.invPrice = invPrice;
    }

    public int getInvStock() {
        return invStock;
    }

    public void setInvStock(int invStock) {
        this.invStock = invStock;
    }

    public String getInvColor() {
        return invColor;
    }

    public void setInvColor(String invColor) {
        this.invColor = invColor;
    }

    public int getClassificationId() {
        return classificationId;
    }

    public void setClassificationId(int classificationId) {
        this.classificationId = classificationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VehicleEntity that = (VehicleEntity) o;

        if (vehicleId != that.vehicleId) return false;
        if (invStock != that.invStock) return false;
        if (classificationId != that.classificationId) return false;
        if (invMake != null ? !invMake.equals(that.invMake) : that.invMake != null) return false;
        if (invModel != null ? !invModel.equals(that.invModel) : that.invModel != null) return false;
        if (invDescription != null ? !invDescription.equals(that.invDescription) : that.invDescription != null)
            return false;
        if (invPrice != null ? !invPrice.equals(that.invPrice) : that.invPrice != null) return false;
        if (invColor != null ? !invColor.equals(that.invColor) : that.invColor != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = vehicleId;
        result = 31 * result + (invMake != null ? invMake.hashCode() : 0);
        result = 31 * result + (invModel != null ? invModel.hashCode() : 0);
        result = 31 * result + (invDescription != null ? invDescription.hashCode() : 0);
        result = 31 * result + (invPrice != null ? invPrice.hashCode() : 0);
        result = 31 * result + invStock;
        result = 31 * result + (invColor != null ? invColor.hashCode() : 0);
        result = 31 * result + classificationId;
        return result;
    }

    public BigDecimal validatePrice(String stringPrice){

        try{
            BigDecimal bdPrice = new BigDecimal(stringPrice).setScale(2, RoundingMode.UP);
            int result = bdPrice.compareTo(BigDecimal.valueOf(0.00));
            if(result == -1){
                bdPrice = null;
            }
            return bdPrice;
        }
        catch (NumberFormatException | ArithmeticException e){
            e.printStackTrace();
            return null;
        }

    }

    public Integer validateStock(String stringStock){

        try {
            Integer intStock = Integer.parseInt(stringStock);
            if(intStock < 0){
                intStock = null;
            }
            return intStock;
        }
        catch (NumberFormatException e){
            e.printStackTrace();
            return null;
        }

    }
}
