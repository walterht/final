package com.example.inventorymanagement;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@WebServlet(name = "Vehicles", value = "/vehicles")
public class Vehicles extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = request.getParameter("action");
        if(action == null){
            action = "";
        }
        DBModels dbModels = DBModels.getInstance();
        BuildHTML buildHTML = new BuildHTML();
        List<ClassificationEntity> classifications = dbModels.getClassifications();
        String navigation = buildHTML.buildNav(classifications);
        request.setAttribute("navigationList", navigation);
        String classificationName;
        int vehicleId;
        VehicleEntity vehicle;

        switch (action){
            case "viewInventory":
                classificationName = request.getParameter("classificationName");
                List<VehicleEntity> vehicleInv = dbModels.getVehiclesByClassification(classificationName);
                String invDisplay = buildHTML.buildInventoryDisplay(vehicleInv);
                request.setAttribute("invDisplay", invDisplay);
                request.setAttribute("classificationName", classificationName);
                request.getRequestDispatcher("views/inventory.jsp").forward(request,response);
            break;
            case "mod":
                vehicleId = Integer.parseInt(request.getParameter("vehicleId"));
                vehicle = dbModels.getVehicleById(vehicleId);
                String classificationList = buildHTML.buildClassificationList(classifications, vehicle.getClassificationId());
                request.setAttribute("classificationList", classificationList);
                request.setAttribute("vehicleId", vehicle.getVehicleId());
                buildHTML.setVehicleAttributes(request, vehicle.getInvMake(), vehicle.getInvModel(), vehicle.getInvDescription(),
                        vehicle.getInvPrice(), vehicle.getInvStock(), vehicle.getInvColor());
                request.getRequestDispatcher("views/vehicle-update.jsp").forward(request, response);
            break;
            case "del":
                vehicleId = Integer.parseInt(request.getParameter("vehicleId"));
                vehicle = dbModels.getVehicleById(vehicleId);
                request.setAttribute("vehicleMake", vehicle.getInvMake());
                request.setAttribute("vehicleModel", vehicle.getInvModel());
                request.setAttribute("description", vehicle.getInvDescription());
                request.setAttribute("vehicleId", vehicle.getVehicleId());
                request.getRequestDispatcher("views/vehicle-delete.jsp").forward(request, response);
            break;
            case "add":
                classificationName = request.getParameter("classificationName");
                for (ClassificationEntity classification:
                        classifications) {
                    if(classification.getClassificationName().equals(classificationName)){
                        request.setAttribute("classificationId", classification.getClassificationId());
                    }
                }
                request.setAttribute("classificationName", classificationName);
                request.getRequestDispatcher("views/add-vehicle.jsp").forward(request, response);
            break;
            default:
                request.getRequestDispatcher("views/instructions.jsp").forward(request, response);
            break;
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = request.getParameter("action");
        DBModels dbModels = DBModels.getInstance();
        BuildHTML buildHTML = new BuildHTML();
        List<ClassificationEntity> classifications = dbModels.getClassifications();
        String navigation = buildHTML.buildNav(classifications);
        request.setAttribute("navigationList", navigation);
        VehicleEntity vehicleEntity;
        String invMake, invModel, invDescription, invColor;
        BigDecimal invPrice;
        Integer invStock;
        int vehicleId, classificationId;

        switch (action){
            case "updateVehicle":
                classificationId = Integer.parseInt(request.getParameter("classificationId"));
                invMake = request.getParameter("invMake").trim();
                invModel = request.getParameter("invModel").trim();
                invDescription = request.getParameter("invDescription").trim();
                //Pass the values for price and stock to a validation function that will make price round to two decimal places
                //and check that none of these two are values below zero. If the values passed are not valid numbers
                //they will return as null
                vehicleEntity = new VehicleEntity();
                invPrice = vehicleEntity.validatePrice(request.getParameter("invPrice").trim());
                invStock = vehicleEntity.validateStock(request.getParameter("invStock").trim());
                invColor = request.getParameter("invColor").trim();
                vehicleId = Integer.parseInt(request.getParameter("vehicleId"));
                //If any of the form fields were empty or did not conform to requirements
                //we will return the data and display an error message requesting all information
                if(invMake.isEmpty() || invModel.isEmpty() || invDescription.isEmpty() ||
                        invPrice == null || invStock == null || invColor.isEmpty()){
                    buildHTML.setVehicleAttributes(request, invMake, invModel, invDescription, invPrice, invStock, invColor);
                    String classificationList = buildHTML.buildClassificationList(classifications, classificationId);
                    request.setAttribute("classificationList", classificationList);
                    request.setAttribute("vehicleId", vehicleId);
                    request.setAttribute("message", "<p>**Please make sure all fields are filled out as requested.**</p>");
                    request.getRequestDispatcher("views/vehicle-update.jsp").forward(request, response);
                    return;
                }
                //Once we've verified all information is correct, we will proceed with updating the record
                vehicleEntity.setVehicleId(vehicleId);
                vehicleEntity.setClassificationId(classificationId);
                vehicleEntity.setInvMake(invMake);
                vehicleEntity.setInvModel(invModel);
                vehicleEntity.setInvDescription(invDescription);
                vehicleEntity.setInvPrice(invPrice);
                vehicleEntity.setInvStock(invStock);
                vehicleEntity.setInvColor(invColor);
                try{
                    dbModels.updateVehicle(vehicleEntity);
                    request.getSession().setAttribute("message", "<p>**" + invMake + " " + invModel +
                            " was successfully modified!**</p>");
                    response.sendRedirect("vehicles");
                }
                catch (Exception e){
                    e.printStackTrace();
                    request.getSession().setAttribute("message", "<p>**There was an error updating the " +
                            invMake + " " + invModel + " vehicle. Try at a later time.**</p>");
                    response.sendRedirect("vehicles");
                }
            break;
            case "deleteVehicle":
                vehicleId = Integer.parseInt(request.getParameter("vehicleId"));
                invMake = request.getParameter("invMake");
                invModel = request.getParameter("invModel");
                vehicleEntity = new VehicleEntity();
                vehicleEntity.setVehicleId(vehicleId);
                try{
                    dbModels.deleteVehicleById(vehicleEntity);
                    request.getSession().setAttribute("message", "<p>**" + invMake + " " + invModel +
                            " was successfully deleted.**</p>");
                    response.sendRedirect("vehicles");
                }
                catch (Exception e){
                    e.printStackTrace();
                    request.getSession().setAttribute("message", "<p>**There was an error deleting the " +
                            invMake + " " + invModel + " vehicle. Try at a later time.**</p>");
                    response.sendRedirect("vehicles");
                }
            break;
            case "addVehicle":
                classificationId = Integer.parseInt(request.getParameter("classificationId"));
                String classificationName = request.getParameter("classificationName");
                invMake = request.getParameter("invMake").trim();
                invModel = request.getParameter("invModel").trim();
                invDescription = request.getParameter("invDescription").trim();
                //Pass the values for price and stock to a validation function that will make price round to two decimal places
                //and check that none of these two are values below zero. If the values passed are not valid numbers
                //they will return as null
                vehicleEntity = new VehicleEntity();
                invPrice = vehicleEntity.validatePrice(request.getParameter("invPrice").trim());
                invStock = vehicleEntity.validateStock(request.getParameter("invStock").trim());
                invColor = request.getParameter("invColor").trim();
                //If any of the form fields were empty or did not conform to requirements
                //we will return the data and display an error message requesting all information
                if(invMake.isEmpty() || invModel.isEmpty() || invDescription.isEmpty() ||
                        invPrice == null || invStock == null || invColor.isEmpty()){
                    buildHTML.setVehicleAttributes(request, invMake, invModel, invDescription, invPrice, invStock, invColor);
                    request.setAttribute("classificationId", classificationId);
                    request.setAttribute("classificationName", classificationName);
                    request.setAttribute("message", "<p>**Please make sure all fields are filled out as requested.**</p>");
                    request.getRequestDispatcher("views/add-vehicle.jsp").forward(request, response);
                    return;
                }
                vehicleEntity.setInvMake(invMake);
                vehicleEntity.setInvModel(invModel);
                vehicleEntity.setInvDescription(invDescription);
                vehicleEntity.setInvPrice(invPrice);
                vehicleEntity.setInvStock(invStock);
                vehicleEntity.setInvColor(invColor);
                vehicleEntity.setClassificationId(classificationId);
                try{
                    dbModels.insertVehicle(vehicleEntity);
                    request.getSession().setAttribute("message", "<p>**" + invMake + " " + invModel +
                            " was successfully added.**</p>");
                    response.sendRedirect("vehicles");
                }
                catch (Exception e){
                    e.printStackTrace();
                    request.getSession().setAttribute("message", "<p>**There was an error adding the " +
                            invMake + " " + invModel + " vehicle. Try at a later time.**</p>");
                    response.sendRedirect("vehicles");
                }
            break;
        }

    }
}
