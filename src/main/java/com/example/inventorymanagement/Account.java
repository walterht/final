package com.example.inventorymanagement;

import org.hibernate.NonUniqueResultException;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "account", value = "/account")
public class Account extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = request.getParameter("action");
        String firstName, lastName, email, password;
        UserEntity user, existingUser;
        DBModels dbModels = DBModels.getInstance();


        switch (action){
            case "register":
                firstName = request.getParameter("clientFirstname").trim();
                lastName = request.getParameter("clientLastname").trim();
                email = request.getParameter("clientEmail").trim();
                password = request.getParameter("clientPassword").trim();
                user = new UserEntity();
                //Validate Email and Password
                email = user.validateEmail(email);
                password = user.validatePassword(password);
                //If any are empty(email and password will be returned as empty if they do not conform to requirements) we will
                //return to the view and display an error message
                if(firstName.isEmpty() || lastName.isEmpty() || email.isEmpty() || password.isEmpty()){
                    request.setAttribute("firstName", firstName);
                    request.setAttribute("lastName", lastName);
                    request.setAttribute("email", email);
                    request.setAttribute("message", "You are missing information or the information does not conform to the requirements. Please submit the information as requested.");
                    request.getRequestDispatcher("views/register.jsp").forward(request, response);
                    return;
                }
                //Check whether an account with the provided email already exists or not.
                try{
                    existingUser = dbModels.checkExistingUser(email);
                    if(existingUser != null){
                        request.setAttribute("firstName", firstName);
                        request.setAttribute("lastName", lastName);
                        request.setAttribute("message", "A user already exists with that email. Please provide a different email");
                        request.getRequestDispatcher("views/register.jsp").forward(request, response);
                        return;
                    }
                }
                catch (NonUniqueResultException e){
                    e.printStackTrace();
                    request.setAttribute("firstName", firstName);
                    request.setAttribute("lastName", lastName);
                    request.setAttribute("message", "A user already exists with that email. Please provide a different email");
                    request.getRequestDispatcher("views/register.jsp").forward(request, response);
                    return;
                }
                //Add the values to the db entity
                user.setUserFirstname(firstName);
                user.setUserLastname(lastName);
                user.setUserEmail(email);
                user.setUserPassword(password);
                //Insert the new record, if it fails and/or an exception is thrown it will return to the register view and display
                //an error message
                try {
                    dbModels.insertUser(user);
                    HttpSession httpSession = request.getSession();
                    httpSession.setAttribute("message", "The account was successfully registered. Please log in to your account to begin managing inventory.");
                    response.sendRedirect("views/login.jsp");
                }
                catch (Exception e){
                    e.printStackTrace();
                    request.setAttribute("firstName", firstName);
                    request.setAttribute("lastName", lastName);
                    request.setAttribute("email", email);
                    request.setAttribute("message", "There was an error registering your account. Please try again later.");
                    request.getRequestDispatcher("views/register.jsp").forward(request, response);
                    return;
                }
            break;
            case "Login":
                email = request.getParameter("clientEmail").trim();
                password = request.getParameter("clientPassword").trim();
                user = new UserEntity();
                //Validate Email and Password
                email = user.validateEmail(email);
                password = user.validatePassword(password);
                //If any are empty(email and password will be returned as empty if they do not conform to requirements) we will
                //return to the view and display an error message
                if(email.isEmpty() || password.isEmpty()){
                    request.setAttribute("email", email);
                    request.setAttribute("message", "You are missing information or the information does not conform to the requirements. Please submit the information as requested.");
                    request.getRequestDispatcher("views/login.jsp").forward(request, response);
                    return;
                }
                //Check whether an account with the provided email exists or not.
                try{
                    existingUser = dbModels.checkExistingUser(email);
                    if(existingUser == null){
                        request.setAttribute("message", "There is no account with that email. Please verify your email, or register for an account.");
                        request.getRequestDispatcher("views/login.jsp").forward(request, response);
                        return;
                    }
                }
                catch (NonUniqueResultException e){
                    e.printStackTrace();
                    return;
                }
                if(!(existingUser.getUserPassword().equals(password))){
                    request.setAttribute("email", email);
                    request.setAttribute("message", "Please check your password and try again.");
                    request.getRequestDispatcher("views/login.jsp").forward(request, response);
                    return;
                }
                request.getSession().setAttribute("LoggedIn", true);
                request.getSession().setAttribute("user", existingUser);
                response.sendRedirect("vehicles");
            break;
            default:
                response.sendRedirect("index.jsp");
            break;
        }

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = request.getParameter("action");
        if(action == null){
            action = "";
        }

        switch (action){
            case "login":
                response.sendRedirect("views/login.jsp");
            break;
            case "registration":
                response.sendRedirect("views/register.jsp");
            break;
            case "logout":
                request.getSession().removeAttribute("LoggedIn");
                request.getSession().removeAttribute("user");
                response.sendRedirect("account");
            break;
            default:
                response.sendRedirect("index.jsp");
            break;
        }

    }

    public void destroy() {
    }
}