package com.example.inventorymanagement;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;

public class BuildHTML {

    public String buildNav(List<ClassificationEntity> navClassifications){

        String navigation = "<ul class=\"nav\"><li><a class='nav-link' href='vehicles' title='View the Inventory Management instructions page'>Instructions</a></li>";
        for (ClassificationEntity navClassification:
                navClassifications) {

            navigation += "<li><a href='vehicles?action=viewInventory&classificationName=" + navClassification.getClassificationName() + "'"
                    + " title='Manage the " + navClassification.getClassificationName() + " vehicles Inventory'>"
                    + navClassification.getClassificationName() + "</a></li>";

        }
        navigation += "</ul>";

        return navigation;

    }

    public  String buildInventoryDisplay(List<VehicleEntity> vehicleInv){

        String invDisplay = "<thead><tr><th>Vehicle Name</th><td>&nbsp;</td><td>&nbsp;</td></tr></thead>";
        invDisplay += "<tbody>";
        for (VehicleEntity vehicle:
             vehicleInv) {

            invDisplay += "<tr><td>" + vehicle.getInvMake() + " " + vehicle.getInvModel() + "</td>";
            invDisplay += "<td><a href='vehicles?action=mod&vehicleId=" + vehicle.getVehicleId() + "'"
                        + " title='Click to modify vehicle'>Modify</a></td>";
            invDisplay += "<td><a href='vehicles?action=del&vehicleId=" + vehicle.getVehicleId() + "'"
                        + " title='Click to delete vehicle'>Delete</a></td></tr>";

        }

        invDisplay += "</tbody>";

        return invDisplay;
    }

    public String buildClassificationList(List<ClassificationEntity> classifications, int currentClassID){

        String classificationList = "<select name='classificationId' id='classificationId'>";
        classificationList += "<option>Choose a Car Classification</option>";
        for (ClassificationEntity classification:
             classifications) {

            classificationList += "<option value='" + classification.getClassificationId() + "'";
            if(classification.getClassificationId() == currentClassID){
                classificationList += " selected";
            }
            classificationList += ">" + classification.getClassificationName() + "</option>";

        }

        classificationList += "</select>";

        return classificationList;
    }

    public void setVehicleAttributes(HttpServletRequest request, String invMake, String invModel, String invDescription,
                                BigDecimal invPrice, Integer invStock, String invColor){

        request.setAttribute("vehicleMake", invMake);
        request.setAttribute("vehicleModel", invModel);
        request.setAttribute("description", invDescription);
        request.setAttribute("price", invPrice);
        request.setAttribute("stock", invStock);
        request.setAttribute("color", invColor);

    }

}
