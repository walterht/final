package com.example.inventorymanagement;

import org.hibernate.NonUniqueResultException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class DBModels {

    SessionFactory factory = null;
    Session session = null;

    private static DBModels single_instance = null;

    private DBModels(){
        factory = HibernateFactory.getSessionFactory();
    }

    public static DBModels getInstance(){
        if(single_instance == null){
            single_instance = new DBModels();
        }

        return single_instance;
    }

    public void insertUser(UserEntity user) throws Exception{

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            session.save(user);
            session.getTransaction().commit();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            throw e;
        }
        finally {
            session.close();
        }

    }

    public UserEntity checkExistingUser(String email) throws NonUniqueResultException{

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "FROM UserEntity WHERE userEmail = '" + email + "'";
            return session.createQuery(query, UserEntity.class).uniqueResult();
        }
        catch (NonUniqueResultException e){
            session.getTransaction().rollback();
            throw e;
        }
        finally {
            session.close();
        }

    }

    public List<ClassificationEntity> getClassifications(){

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "FROM ClassificationEntity";
            return session.createQuery(query, ClassificationEntity.class).getResultList();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        finally {
            session.close();
        }

        return null;
    }

    public List<VehicleEntity> getVehiclesByClassification(String classificationName){

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "FROM VehicleEntity WHERE classificationId = (SELECT classificationId FROM ClassificationEntity WHERE classificationName = '"
                            +  classificationName + "')";
            return session.createQuery(query, VehicleEntity.class).getResultList();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        finally {
            session.close();
        }

        return null;
    }

    public VehicleEntity getVehicleById(int vehicleId){

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "FROM VehicleEntity WHERE vehicleId = " + vehicleId;
            return session.createQuery(query, VehicleEntity.class).getSingleResult();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        finally {
            session.close();
        }

        return null;
    }

    public void updateVehicle(VehicleEntity updatedVehicle)throws Exception{

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            session.update(updatedVehicle);
            session.getTransaction().commit();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            throw e;
        }
        finally {
            session.close();
        }

    }

    public void deleteVehicleById(VehicleEntity vehicleToDelete) throws Exception{

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            session.delete(vehicleToDelete);
            session.getTransaction().commit();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            throw e;
        }
        finally {
            session.close();
        }

    }

    public void insertVehicle(VehicleEntity newVehicle) throws Exception{

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            session.save(newVehicle);
            session.getTransaction().commit();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            throw e;
        }
        finally {
            session.close();
        }

    }

}
