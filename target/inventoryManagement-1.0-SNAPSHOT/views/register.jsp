<%--
  Created by IntelliJ IDEA.
  User: walta
  Date: 3/23/2022
  Time: 9:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Register | Inventory Management</title>
    <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/home.css">
</head>
<body>

    <%@ include file="../snippets/header.jsp"%>

    <nav></nav>

    <main>
        <h1>Register Now!</h1>

        <c:if test="${requestScope.message != null}">
            <p>${requestScope.message}</p>
        </c:if>
        <form method="post" action="${pageContext.request.contextPath}/account">
            <label for="fName">*First Name:</label>
            <input name="clientFirstname" id="fName" type="text" value="${requestScope.firstName}" required>
            <label for="lName">*Last Name:</label>
            <input name="clientLastname" id="lName" type="text" value="${requestScope.lastName}" required>
            <label for="email">*Email:</label>
            <input name="clientEmail" id="email" type="email" value="${requestScope.email}" required>
            <label for="password">*Password:</label>
            <span>Passwords must be at least 8 characters and contain at least 1 number, 1 capital letter and 1 special character</span>
            <input name="clientPassword" id="password" type="password" required pattern="(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">
            <input type="submit" name="submit" id="form_btn" value="Create Account">
            <input type="hidden" name="action" value="register">
        </form>
        <p>*These fields are required.</p>
    </main>

    <%@ include file="../snippets/footer.jsp"%>
</body>
</html>
