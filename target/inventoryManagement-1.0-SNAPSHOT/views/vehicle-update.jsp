<%--
  Created by IntelliJ IDEA.
  User: walta
  Date: 3/28/2022
  Time: 9:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Modify ${requestScope.vehicleMake} ${requestScope.vehicleModel} | Inventory Management</title>
  <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@500&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/home.css">
</head>
<body>

  <%@ include file="../snippets/header.jsp"%>

  <nav>
    <c:if test="${requestScope.navigationList != null}">
      ${requestScope.navigationList}
      <c:remove var="navigationList" scope="request"/>
    </c:if>
  </nav>

  <main>
    <h1>Modify ${requestScope.vehicleMake} ${requestScope.vehicleModel}</h1>
    <c:if test="${requestScope.message != null}">
        ${requestScope.message}
        <c:remove var="message" scope="request"/>
    </c:if>

    <p>*All Fields Are Required</p>
    <form method="post" action="${pageContext.request.contextPath}/vehicles">
        ${requestScope.classificationList}
        <label for="invMake">Make:</label>
        <input type="text" name="invMake" id="invMake" value="${requestScope.vehicleMake}" required>
        <label for="invModel">Model:</label>
        <input type="text" name="invModel" id="invModel" value="${requestScope.vehicleModel}" required>
        <label for="invDescription">Description:</label>
        <textarea name="invDescription" id="invDescription" required>${requestScope.description}</textarea>
        <label for="invPrice">Price:</label>
        <input type="number" min="0" step="0.01" name="invPrice" id="invPrice" value="${requestScope.price}" required>
        <label for="invStock">Amount in Stock:</label>
        <input type="number" min="0" step="1" name="invStock" id="invStock" value="${requestScope.stock}" required>
        <label for="invColor">Color:</label>
        <input type="text" name="invColor" id="invColor" value="${requestScope.color}" required>
        <input type="submit" name="submit" id="form_btn" value="Update Vehicle">
        <input type="hidden" name="action" value="updateVehicle">
        <input type="hidden" name="vehicleId" value="${requestScope.vehicleId}">
    </form>
  </main>

  <%@ include file="../snippets/footer.jsp"%>

</body>
</html>
