<%--
  Created by IntelliJ IDEA.
  User: walta
  Date: 3/23/2022
  Time: 9:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Instructions | Inventory Management</title>
    <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/home.css">
</head>
<body>

    <%@ include file="../snippets/header.jsp"%>

    <nav>
        <c:if test="${requestScope.navigationList != null}">
            ${requestScope.navigationList}
            <c:remove var="navigationList" scope="request"/>
        </c:if>
    </nav>

    <main>
        <h1>Welcome ${sessionScope.user.getUserFirstname()}</h1>

        <c:if test="${sessionScope.message != null}">
            ${sessionScope.message}
            <c:remove var="message" scope="session"/>
        </c:if>

        <p>
            To manage the vehicle inventory, select the classification of vehicles for which you wish to manage the inventory.
            This will show you all vehicles within that classification, and you will be presented with options to
            update or delete a vehicle currently in inventory, or add a new vehicle to the classification.
        </p>
        <p>When you're finished, don't forget to Log Out.</p>
    </main>

    <%@ include file="../snippets/footer.jsp"%>
</body>
</html>
